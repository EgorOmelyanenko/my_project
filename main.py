# IMPORTANT: now API accessible from container only when docker network isolation disabled
# iptables -I INPUT -i br-ALL -j ACCEPT
# br-ALL = all docker virtual networks (to simplify appropriate network lookup)

import asyncio
import functools
import logging
import signal

from aiohttp.web import Application as Server
from aiohttp.web import run_app

from api.application import ApplicationApi as application
from api.command import CommandApi as command
from api.information import InformationApi as information
from python_toolkit import filesystem
from python_toolkit.cli.input_noninteractive import input_command
from python_toolkit.log import Log, get_log, set_log

from platform_controller.interfaces import get_docker_host_ip


def start(args):
    application.initialize(
        args["architecture"],
        args["development_localhost_flag"],
        args["development_mount_source_code_flag"],
        args["configurator_web_tag"],
        args["watcher_network_tag"],
        args["watcher_logs_tag"],
        args["connector_tag"]
    )

    if not application.start__application():
        return

    try:
        get_log().debug("Starting API server.")
        run_server()
    except:
        get_log().error("Failed to start API server.")
        return


def stop():
    get_log().debug("Interruption signal received. Stopping.")

    asyncio.get_event_loop().stop()

    command.stop_all__command()
    application.stop__application()


def run_server(port=8080, handle_signals=False):
    event_loop = asyncio.get_event_loop()
    event_loop.add_signal_handler(signal.SIGINT, functools.partial(stop))
    event_loop.add_signal_handler(signal.SIGTERM, functools.partial(stop))

    server = Server()
    init_handlers(server)
    run_app(server, host=("127.0.0.1", get_docker_host_ip()), port=port, handle_signals=handle_signals)
    # TODO: log server started


def init_handlers(server):

    # component

    server.router.add_get(
        "/component/start",
        application.start__component
    )

    server.router.add_get(
        "/component/stop",
        application.stop__component
    )

    server.router.add_get(
        "/component/remove",
        application.remove__component
    )

    server.router.add_get(
        "/component/apply-configuration",
        application.apply_configuration__component
    )

    # command

    server.router.add_get(
        "/command/start",
        command.start__command
    )

    server.router.add_get(
        "/command/stop",
        command.stop__command
    )

    server.router.add_get(
        "/command/buffer",
        command.get_buffer__command
    )

    # information

    server.router.add_get(
        "/information/network-interfaces",
        information.information__network_interfaces
    )

    server.router.add_get(
        "/information/dhcp-leases",
        information.information__dhcp_leases
    )


def main():
    # TODO: read log level from configuration
    # TODO: log to file
    filesystem.initialize()

    set_log(Log(Log.DEBUG))
    # TODO: disable urllib3 logging cleaner
    logging.getLogger("urllib3").setLevel(Log.CRITICAL)

    get_log().debug("Starting.")

    input_command(
        "Starting/stopping service.",
        (
            # service
            (
                "start", start, (
                    ("architecture", {"type": str}),
                    ("development_localhost_flag", {"type": int}),
                    ("development_mount_source_code_flag", {"type": int}),
                    ("configurator_web_tag", {"type": str}),
                    ("watcher_network_tag", {"type": str}),
                    ("watcher_logs_tag", {"type": str}),
                    ("connector_tag", {"type": str}),
                )
            ),
        )
    )()


if __name__ == "__main__":
    main()
