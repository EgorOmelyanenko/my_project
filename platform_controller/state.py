from python_toolkit.filesystem import configuration_read, configuration_write
from platform_controller.interfaces import get_configuration as get_interfaces


class State:

    STATE = None
    FILE = None

    @staticmethod
    def get_state():
        return State.STATE

    @staticmethod
    def update_state(content):
        State.get_state().update(content)

    @staticmethod # decorator for methods that must to rewriting configuration
    def write_state(func):

        def wrapped(*args, **kwargs):
            result = func(*args, **kwargs)
            State._write_state()
            return result

        return wrapped

    @staticmethod
    def init_state(file_="state"):
        State.FILE = file_
        State.STATE = configuration_read(file=State.FILE)

        if not State.STATE:
            State.STATE = {
                "components": {},
                "interfaces": get_interfaces()
            }

            State._write_state()

    @staticmethod
    def _write_state():
        configuration_write(file=State.FILE, content=State.get_state())
