import asyncio


async def execute(command):
    process = await asyncio.create_subprocess_shell(
        command,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE
    )
    stdout, stderr = await process.communicate()

    return {
        "exit_code": process.returncode,
        "stdout": stdout.decode("utf-8"),
        "stderr": stderr.decode("utf-8"),
    }
