import re
import subprocess

import netifaces

def get_docker_host_ip(network="docker0"):
    return _get_ip(network, netifaces.AF_INET)["address"]

def get_configuration():
    result = {
        iface: {
            "mac": _get_info(iface, "address"),
            "broadcast": _get_info(iface, "broadcast"),
            "state": _get_info(iface, "operstate"),
            "type": _get_type(iface),
            "ipv4": _get_ip(iface, netifaces.AF_INET),
            "ipv6": _get_ip(iface, netifaces.AF_INET6),
        } for iface in _get_physical_ifaces()
    }
    result.update({"dns": _get_dns()})
    return result

def _get_physical_ifaces():
    all_ifaces = subprocess.check_output(["ls", "/sys/class/net/"]).decode().split()
    virtual_ifaces = subprocess.check_output(["ls", "/sys/devices/virtual/net/"]).decode().split()
    yield from [iface for iface in all_ifaces if iface not in virtual_ifaces]

def _get_info(iface, param):
    with open("/sys/class/net/{}/{}".format(iface, param), "r") as f:
        return f.read().rstrip()

def _get_type(iface):
    if "eth" in iface or "eno" in iface:
        return "wired"
    if "wlan" in iface or "wlo" in iface:
        return "wireless"
    return "unknown"

def _get_ip(iface, version):
    if version in netifaces.ifaddresses(iface).keys():
        ifaddresses = netifaces.ifaddresses(iface)[version][0]
        return {
            "address":
                ifaddresses["addr"] if version == netifaces.AF_INET
                else re.findall(r".+(?=%)", ifaddresses["addr"]), # remove zone identifier from ipv6 addr
            "netmask": ifaddresses["netmask"],
            "gateway": _get_gateway(iface, version)
        }

def _get_gateway(iface, version):
    if version in netifaces.gateways().keys():
        for gateway in netifaces.gateways()[version]:
            if iface in gateway:
                return {
                    "address": gateway[0],
                    "default": gateway[2]
                }

def _get_dns():
    with open("/etc/resolv.conf", "r") as f:
        lines = f.read()
        return {
            "ipv4": re.findall(r"(?<=nameserver\s)([^:\n]+)\n", lines),
            "ipv6": re.findall(r"(?<=nameserver\s)([^.\n]+)\n", lines)
        }
