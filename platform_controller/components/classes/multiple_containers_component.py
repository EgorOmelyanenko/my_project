from platform_controller.components.classes.container_component import ContainerComponent
from platform_controller.container import DockerContainer
from python_toolkit.log import get_log


class MultipleContainersComponent(ContainerComponent):
    def __init__(
            self,
            image,
            tag,
            entrypoint=None, # list
            volumes=None, # dict
            networks=None, # lsit
            devices=None, # lsit
            network_mode=None,
            privileged=False,
            ports=None, # dict
            startup_commands=None, # lsit
            cap_add=None, # lsit
        ):
        ContainerComponent.__init__(
            self,
            image=image,
            tag=tag,
            entrypoint=entrypoint,
            volumes=volumes,
            networks=networks,
            devices=devices,
            network_mode=network_mode,
            privileged=privileged,
            ports=ports,
            startup_commands=startup_commands,
            cap_add=cap_add
        )
        self.i_max_containers_count = 0

    def start(self, id_):
        if self.i_max_containers_count != 0 and len(self.i_configuration) + 1 > self.i_max_containers_count:
            get_log().debug("Impossibly to start {} component. Reached the maximum ({}) count of containers.".format(
                self.get_name(id_),
                self.i_max_containers_count
            ))
            return False

        return ContainerComponent.start(self, id_=id_)

    def start_from_state(self):
        results = []

        for id_ in self.i_configuration:
            if self.i_configuration[id_]["started_flag"]:
                get_log().debug("Starting component {}".format(self.get_name(id_)))

                if self.i_configuration[id_]["container"].start():
                    get_log().debug("Component {} started.".format(self.get_name(id_)))
                    results.append(True)

                get_log().debug("Failed to start component {}".format(self.get_name(id_)))
            results.append(False)

        return False not in results

    def stop(self, id_=None):
        ids = [id_] if id_ else self.i_configuration
        return False not in [ContainerComponent.stop(self, id_=subcomponent_id) for subcomponent_id in ids]

    def is_started(self, id_):
        return self.i_configuration[id_]["started_flag"] if id_ in self.i_configuration else False

    def set_max_containers_count(self, count):
        self.i_max_containers_count = count

    def get_name(self, id_=None):
        if id_:
            return "{}_{}".format(self.get_name(), id_)
        return super().get_name()

    # private

    def _get_container(self, id_):
        return self.i_configuration[id_]["container"] if id_ in self.i_configuration else None

    def _get_volumes(self, id_):
        return {"{}_{}".format(key, id_): item for key, item in (self.i_volumes or {}).items()} # volumes names to "name_id" form

    def _configuration_write(self, key, value, id_):
        if id_ not in self.i_configuration:
            self.i_configuration[id_] = {}

        self.i_configuration[id_][key] = value
        self._update_state()

    def _configuration_remove(self, id_):
        self.i_configuration.pop(id_)
        self._update_state()
