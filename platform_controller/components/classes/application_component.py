from platform_controller.components.classes.container_component import ContainerComponent
from platform_controller.container import DockerContainer
from python_toolkit.log import get_log


class ApplicationComponent(ContainerComponent):
    def __init__(
            self,
            image,
            tag,
            entrypoint=None, # list
            volumes=None, # dict
            networks=None, # lsit
            network_mode=None,
            ports=None, # dict
            startup_commands=None, # lsit
        ):

        ContainerComponent.__init__(
            self,
            image=image,
            tag=tag,
            entrypoint=entrypoint,
            volumes=volumes,
            networks=networks,
            network_mode=network_mode,
            ports=ports,
            startup_commands=startup_commands
        )
        self.i_container = None

    def start(self):
        get_log().debug("Starting application component {}".format(self.get_name()))

        if self.i_container:
            get_log().debug("Application component {} already started".format(self.get_name()))
            return False

        self.i_container = DockerContainer(
            image="gitlab.nii-ikt.ru/ark_gateway/{}.{}".format(
                self.i_image,
                self.c_arhitecture,
            ),
            tag=self.i_tag,
            name=self.i_name,
            networks=self.i_networks,
            network_mode=self.i_network_mode,
            volumes=self.i_volumes,
            entrypoint=self.i_entrypoint,
            ports=self.i_ports,
            startup_commands=self.i_startup_commands,
        )

        if self.i_container.start():
            get_log().debug("Application component {} started".format(self.get_name()))
            return True

        return False

    def stop(self):
        get_log().debug("Stopping application component {}".format(self.get_name()))

        if self.i_container and self.i_container.stop():
            get_log().debug("Application component {} stopped".format(self.get_name()))
            self.i_container = None
            return True

        get_log().debug("Failed to stop application component {}".format(self.get_name()))
        return False

    async def apply_configuration(self):
        raise NotImplementedError()
