from platform_controller.components.classes.base_component import BaseComponent
from python_toolkit.filesystem import get_project_directory


class HostComponent(BaseComponent):
    def is_started(self):
        return True

    async def apply_configuration(self, configuration):
        return True if self.c_development_localhost_flag else await super().apply_configuration(configuration)

    # private

    def _get_ansible_params(self):
        return (
            "ansible/ansible/host",
            "localhost",
            "local",
            self.get_name(),
            "ansible/playbook.yaml"
        )
