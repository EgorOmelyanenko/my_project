from platform_controller.components.classes.base_component import BaseComponent
from platform_controller.container import DockerContainer
from python_toolkit.log import get_log


class ContainerComponent(BaseComponent):
    def __init__(
            self,
            image,
            tag,
            entrypoint=None, # list
            volumes=None, # dict
            networks=None, # lsit
            devices=None, # lsit
            network_mode=None,
            privileged=False,
            ports=None, # dict
            startup_commands=None, # lsit
            cap_add=None, # lsit
        ):

        super().__init__()

        self.i_image = image
        self.i_tag = tag
        self.i_entrypoint = entrypoint
        self.i_volumes = volumes
        self.i_networks = networks
        self.i_devices = devices
        self.i_network_mode = network_mode
        self.i_privileged = privileged
        self.i_ports = ports
        self.i_startup_commands = startup_commands
        self.i_cap_add = cap_add

    def start(self, **kwargs):
        get_log().debug("Starting component {}".format(self.get_name(**kwargs)))

        if self.is_started(**kwargs):
            get_log().debug("Component {} already started".format(self.get_name(**kwargs)))
            return False

        container = DockerContainer(
            image="gitlab.nii-ikt.ru/ark_gateway/{}.{}".format(
                self.i_image,
                self.c_arhitecture,
            ),
            tag=self.i_tag,
            name=self.get_name(**kwargs),
            networks=self.i_networks,
            devices=self.i_devices,
            network_mode=self.i_network_mode,
            privileged=self.i_privileged,
            volumes=self._get_volumes(**kwargs),
            entrypoint=self.i_entrypoint,
            ports=self.i_ports,
            startup_commands=self.i_startup_commands,
            cap_add=self.i_cap_add
        )

        if container.start():
            self._configuration_write("container", container, **kwargs)
            self._configuration_write("started_flag", True, **kwargs)
            get_log().debug("Component {} started.".format(self.get_name(**kwargs)))
            return True

        get_log().debug("Failed to start component {}".format(self.get_name(**kwargs)))
        container.stop()
        return False


    def start_from_state(self):
        if self.i_configuration["started_flag"]:
            get_log().debug("Starting component {}".format(self.get_name()))

            if self.i_configuration["container"].start():
                get_log().debug("Component {} started.".format(self.get_name()))
                return True

        get_log().debug("Failed to start component {}".format(self.get_name()))
        return False

    def stop(self, **kwargs):
        if not self.is_started(**kwargs):
            return False

        get_log().debug("Stopping component {}".format(self.get_name(**kwargs)))
        if not self._get_container(**kwargs).stop():
            get_log().debug("Failing stopping component {}".format(self.get_name(**kwargs)))
            return False

        self._configuration_write("started_flag", False, **kwargs)
        get_log().debug("Stopped component {}".format(self.get_name(**kwargs)))
        return True

    def remove(self, **kwargs):

        get_log().debug("Removing component {}".format(self.get_name(**kwargs)))

        if self.is_started(**kwargs):
            self.stop(**kwargs)

        if self._get_container(**kwargs) and self._get_container(**kwargs).remove():
            DockerContainer.cleanup(self)

            get_log().debug("Removed component {}".format(self.get_name(**kwargs)))
            self._configuration_remove(**kwargs)
            return True

        get_log().debug("Failing removing component {}".format(self.get_name(**kwargs)))
        return False

    # private

    def _get_ansible_params(self, **kwargs):
        return (
            "ansible/ansible/docker",
            self._get_container(**kwargs).get_id(),
            "docker",
            self.get_name(),
            "ansible/playbook.yaml"
        )

    def _get_container(self, **kwargs):
        raise NotImplementedError()

    def _get_volumes(self, **kwargs):
        raise NotImplementedError()
