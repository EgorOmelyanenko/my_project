import pickle
import re

from platform_controller.ansible_configurator import AnsibleConfigurator
from platform_controller.state import State
from python_toolkit.log import get_log


class BaseComponent:
    c_arhitecture = None
    c_development_localhost_flag = None
    c_development_mount_source_code_flag = None

    def __init__(self):
        self.i_name = re.sub(
            "([a-z])([A-Z])",
            r"\1_\2",
            self.__class__.__name__.replace("Component", "")
        ).lower()
        self.i_configuration = {}

    @classmethod
    def initialize(cls, arhitecture, development_localhost_flag, development_mount_source_code_flag):
        cls.c_arhitecture = arhitecture
        cls.c_development_localhost_flag = development_localhost_flag
        cls.c_development_mount_source_code_flag = development_mount_source_code_flag

    def get_name(self, **kwargs):
        return self.i_name

    def start(self, **kwargs):
        raise NotImplementedError()

    def stop(self, **kwargs):
        raise NotImplementedError()

    async def apply_configuration(self, configuration, **kwargs):
        if not self.is_started(**kwargs):
            get_log().debug("Component {} doesn't started".format(self.get_name(**kwargs)))
            return False

        result = await AnsibleConfigurator(*self._get_ansible_params(**kwargs)).apply_configuration(configuration)

        if result["exit_code"] != 0:
            get_log().error(result["stdout"])
            get_log().error(result["stderr"])
            return False

        get_log().debug(result["stdout"])
        get_log().debug(result["stderr"])

        self._configuration_write("ansible_configuration", configuration, **kwargs)
        return True

    def is_started(self, **kwargs):
        raise NotImplementedError()

    # private

    def _configuration_write(self, key, value):
        self.i_configuration[key] = value
        self._update_state()


    def _configuration_remove(self):
        self.i_configuration = {}
        self._update_state()

    def _get_ansible_params(self, **kwargs):
        raise NotImplementedError()

    def _update_state(self):
        State.update_state({
            "components": {
                self.i_name: pickle.dumps(self)
            }
        })
