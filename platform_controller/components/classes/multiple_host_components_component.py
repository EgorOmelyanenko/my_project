from platform_controller.components.classes.host_component import HostComponent


class MultipleHostComponentsComponent(HostComponent):

    def __init__(self, subcomponents):
        super().__init__()
        self.i_subcomponents = {}
        self.i_subcomponents_priority_for_applying = [] # NB: that list sets sequence of applying configuration for components

        for subcomponent in subcomponents:
            self.i_subcomponents[subcomponent.get_name()] = subcomponent
            self.i_subcomponents_priority_for_applying.append(subcomponent.get_name())

    async def apply_configuration(self, configuration):
        if self.c_development_localhost_flag:
            return True

        result_list = []

        for subcomponent_name in self.i_subcomponents_priority_for_applying:
            if subcomponent_name in configuration and configuration[subcomponent_name]:
                result_list.append(
                    await self.i_subcomponents[subcomponent_name].apply_configuration(
                        {
                            subcomponent_name: configuration[subcomponent_name]
                        }
                    )
                )

        return False not in result_list
