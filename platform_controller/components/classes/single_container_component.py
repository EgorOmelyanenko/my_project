from platform_controller.components.classes.container_component import ContainerComponent
from platform_controller.container import DockerContainer
from python_toolkit.log import get_log


class SingleContainerComponent(ContainerComponent):
    def __init__(
            self,
            image,
            tag,
            entrypoint=None, # list
            volumes=None, # dict
            networks=None, # lsit
            devices=None, # lsit
            network_mode=None,
            privileged=False,
            ports=None, # dict
            startup_commands=None, # lsit
            cap_add=None, # lsit
        ):
        ContainerComponent.__init__(
            self,
            image=image,
            tag=tag,
            entrypoint=entrypoint,
            volumes=volumes,
            networks=networks,
            devices=devices,
            privileged=privileged,
            network_mode=network_mode,
            ports=ports,
            startup_commands=startup_commands,
            cap_add=[]
        )

        self.i_configuration = {
            "started_flag": False,
            "container": None
        }

    def is_started(self):
        return self.i_configuration["started_flag"] if self.i_configuration else False

    # private

    def _get_container_name(self):
        return self.get_name()

    def _get_container(self):
        return self.i_configuration["container"] if self.i_configuration else None

    def _get_volumes(self):
        return self.i_volumes
