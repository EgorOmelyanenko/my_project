from pathlib import Path
from platform_controller.components.classes.application_component import ApplicationComponent
from platform_controller.components.classes.base_component import BaseComponent


class WatcherNetworkComponent(ApplicationComponent):
    def __init__(self, tag):
        super().__init__(
            "watcher_network/application",
            tag,
            entrypoint=["/usr/bin/python3", "-u", "/usr/bin/ark_gateway/watcher_network/main.py"],
            volumes={
                "/var/log/dnsmasq.log": {
                    "bind": "/var/log/dnsmasq.log",
                    "mode": "ro",
                },
                "watcher_network.log": {
                    "bind": "/usr/bin/ark_gateway/watcher_network/log",
                    "mode": "rw",
                },
                "{}/workspace/ark_gateway/watcher_network".format(Path.home()): {
                    "bind": "/usr/bin/ark_gateway/watcher_network",
                    "mode": "rw",
                }
            } if BaseComponent.c_development_mount_source_code_flag else{
                "watcher_network.log": {
                    "bind": "/usr/bin/ark_gateway/watcher_network/log",
                    "mode": "rw",
                },
                "/var/log/dnsmasq.log": {
                    "bind": "/var/log/dnsmasq.log",
                    "mode": "ro",
                },
            },
            network_mode="host",
            # TODO: startup command
        )
