from pathlib import Path
from platform_controller.components.classes.application_component import ApplicationComponent
from platform_controller.components.classes.base_component import BaseComponent


class ConnectorComponent(ApplicationComponent):
    def __init__(self, tag):
        super().__init__(
            "connector/application",
            tag,
            entrypoint=[
                "/usr/bin/python3", "-u", "/usr/bin/ark_gateway/connector/main.py",
            ],
            volumes={
                "{}/workspace/ark_gateway/connector".format(Path.home()): {
                    "bind": "/usr/bin/ark_gateway/connector",
                    "mode": "rw",
                }
            } if BaseComponent.c_development_mount_source_code_flag else {}
            # TODO: startup command
        )
