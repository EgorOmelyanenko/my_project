from pathlib import Path
from platform_controller.components.classes.base_component import BaseComponent
from platform_controller.components.classes.application_component import ApplicationComponent


class ConfiguratorWebApplicationComponent(ApplicationComponent):
    def __init__(self, tag):
        super().__init__(
            "configurator_web/application",
            tag,
            entrypoint=[
                "/usr/bin/uwsgi", "--ini", "/etc/uwsgi/uwsgi.ini",
            ],
            volumes={
                "{}/workspace/ark_gateway/configurator_web".format(Path.home()): {
                    "bind": "/usr/bin/ark_gateway/configurator_web",
                    "mode": "rw",
                }
            } if BaseComponent.c_development_mount_source_code_flag else {},
            networks=[
                "postgresql",
                "redis",
                "uwsgi",
            ],
        )
        self.i_name = "configurator_web__application__flask"
