from pathlib import Path
from platform_controller.components.classes.application_component import ApplicationComponent
from platform_controller.components.classes.base_component import BaseComponent


class WatcherLogsComponent(ApplicationComponent):
    def __init__(self, tag):
        super().__init__(
            "watcher_logs/application",
            tag,
            entrypoint=["/usr/bin/python3", "-u", "/usr/bin/ark_gateway/watcher_logs/main.py"],
            volumes={
                "{}/workspace/ark_gateway/watcher_logs".format(Path.home()): {
                    "bind": "/usr/bin/ark_gateway/watcher_logs",
                    "mode": "rw",
                }
            } if BaseComponent.c_development_mount_source_code_flag else {},
            networks=[
                "postgresql",
            ]
            # TODO: startup command
        )
