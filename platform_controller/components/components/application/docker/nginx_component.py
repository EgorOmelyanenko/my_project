from platform_controller.components.classes.application_component import ApplicationComponent


class NginxComponent(ApplicationComponent):
    def __init__(self, tag):
        super().__init__(
            "configurator_web/ark_gateway-nginx",
            tag,
            volumes={
                "nginx.configuration": {
                    "bind": "/etc/nginx",
                    "mode": "rw",
                },
                "nginx.logs": {
                    "bind": "/var/log/nginx",
                    "mode": "rw",
                },
                "nginx.data": {
                    "bind": "/var/www",
                    "mode": "rw",
                },
            },
            networks=[
                "uwsgi",
            ],
            ports={
                "8080/tcp": 80,
                "443/tcp": 443,
            }
        )
