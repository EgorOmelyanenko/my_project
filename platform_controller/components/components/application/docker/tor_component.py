from platform_controller.components.classes.application_component import ApplicationComponent


class TorApplicationComponent(ApplicationComponent):
    def __init__(self, tag):
        super().__init__(
            "configurator_web/ark_gateway-tor",
            tag,
            volumes={
                "tor.logs": {
                    "bind": "/var/log/tor",
                    "mode": "rw",
                },
                "tor.data.var.lib": {
                    "bind": "/var/lib/tor",
                    "mode": "rw",
                },
                "tor.data.etc": {
                    "bind": "/etc/tor",
                    "mode": "rw",
                },
            },
            networks=[
                "postgresql",
            ],
            # startup_commands=["curl --socks5-hostname 127.0.0.1:9050 yandex.ru --connect-timeout 10"] # ERROR: (7) Failed to receive SOCKS5 connect request ack
        )
        self.i_name = "tor"
