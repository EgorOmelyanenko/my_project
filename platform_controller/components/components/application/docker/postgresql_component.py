from platform_controller.components.classes.application_component import ApplicationComponent


class PostgresqlComponent(ApplicationComponent):
    def __init__(self, tag):
        super().__init__(
            "configurator_web/ark_gateway-postgresql",
            tag,
            volumes={
                "postgresql.data": {
                    "bind": "/var/lib/postgresql",
                    "mode": "rw",
                },
            },
            networks=[
                "postgresql",
            ],
            ports={
                "5432/tcp": 5432,
            },
            startup_commands=["psql --username postgres --host 127.0.0.1 -c 'SHOW ALL;'"]
        )
