from platform_controller.components.classes.application_component import ApplicationComponent


class RedisComponent(ApplicationComponent):
    def __init__(self, tag):
        super().__init__(
            "configurator_web/ark_gateway-redis",
            tag,
            volumes={
                "redis.data": {
                    "bind": "/var/lib/redis",
                    "mode": "rw",
                },
            },
            ports={
                "6379/tcp": 6379, # thats need for watcher_network's access from 'host' network
            },
            networks=[
                "redis",
            ]
            # startup_command
        )
