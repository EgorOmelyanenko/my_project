from platform_controller.components.classes.single_container_component import SingleContainerComponent

class OpenvpnSeverBridgeComponent(SingleContainerComponent):
    def __init__(self, tag):
        super().__init__(
            "configurator_web/ark_gateway-openvpn_server_bridge",
            tag,
            entrypoint=["/usr/sbin/openvpn", "--cd", "/etc/openvpn", "--config", "server.conf"],
            devices=["/dev/net/tun"],
            cap_add=["NET_ADMIN"],
            network_mode="host",
            privileged=True
        )

        self.i_name = "openvpn_server_bridge"
