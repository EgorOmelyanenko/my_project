from platform_controller.components.classes.multiple_containers_component import MultipleContainersComponent

class OpenvpnClientComponent(MultipleContainersComponent):
    def __init__(self, tag):
        super().__init__(
            "configurator_web/ark_gateway-openvpn_client",
            tag,
            devices=["/dev/net/tun"],
            privileged=True,
            entrypoint=["/usr/sbin/openvpn", "--cd", "/etc/openvpn", "--config", "client.conf"],
            cap_add=["NET_ADMIN"]
        )

        self.i_name = "openvpn_client"

        self.i_max_containers_count = 0
