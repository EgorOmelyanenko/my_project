from platform_controller.components.classes.multiple_containers_component import MultipleContainersComponent


class TorComponent(MultipleContainersComponent):
    def __init__(self, tag):
        super().__init__(
            "configurator_web/ark_gateway-tor",
            tag,
            volumes={
                "tor.logs": {
                    "bind": "/var/log/tor",
                    "mode": "rw",
                },
                "tor.data.var.lib": {
                    "bind": "/var/lib/tor",
                    "mode": "rw",
                },
                "tor.data.etc": {
                    "bind": "/etc/tor",
                    "mode": "rw",
                },
            },
            networks=[
                "postgresql",
            ]
            # DEFECT: configurator/issues/7
            # startup_commands=["curl --socks5-hostname 127.0.0.1:9050 yandex.ru --connect-timeout 10"]
        )
        self.i_max_container_count = 0
