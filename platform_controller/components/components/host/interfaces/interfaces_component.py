from platform_controller import interfaces
from platform_controller.components.classes.multiple_host_components_component import MultipleHostComponentsComponent
from platform_controller.components.components.host.interfaces.subcomponents.network_interfaces import NetworkInterfacesComponent
from platform_controller.components.components.host.interfaces.subcomponents.wireless_bridge_interfaces import WirelessBridgeInterfacesComponent
from platform_controller.components.components.host.interfaces.subcomponents.wireless_client import WirelessClientComponent
from platform_controller.components.components.host.interfaces.subcomponents.wireless_hotspot_interfaces import WirelessHotspotInterfacesComponent


# interface component manages wired, wireless bridge, hotspot and client subcomponents

class InterfacesComponent(MultipleHostComponentsComponent):

    def __init__(self):
        super().__init__([
            WirelessBridgeInterfacesComponent(),
            WirelessHotspotInterfacesComponent(),
            WirelessClientComponent(),
            NetworkInterfacesComponent(),
        ])
