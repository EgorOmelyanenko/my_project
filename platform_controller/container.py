import time

import docker as docker_py


DOCKER = docker_py.from_env()

def try_except(func):
    def wrapper(*args, **kwargs):
        try:
            func(*args, **kwargs)
        except:
            return False
        return True
    return wrapper


class DockerContainer:
    def __init__(
            self,
            image,
            tag,
            name,
            entrypoint=None, # list
            volumes=None, # dict
            networks=None, # lsit
            devices=None, # lsit
            network_mode=None,
            privileged=False,
            ports=None, # dict
            startup_commands=None, # lsit
            cap_add=None, # lsit
        ):
        self.i_name = name
        self.i_startup_commands = startup_commands
        self.i_networks = networks

        if not self._get_yourself():
            DockerContainer.pull_image("{}:{}".format(image, tag)) # TODO: remove line in product version
            DOCKER.containers.create(
                image="{}:{}".format(image, tag),
                name=name,
                volumes=volumes,
                entrypoint=entrypoint,
                ports=ports,
                network_mode=network_mode,
                devices=["{0}:{0}:rwm".format(device) for device in devices or ()],
                privileged=privileged,
                cap_add=cap_add,
                detach=True,
            )
        if not network_mode: # "network" and "network_mode" params is incompatible
            self.attach_networks(networks)

    def start(self):
        self._get_yourself().start()
        if self.i_startup_commands:
            return self._execute_startup_commands()
        return True

    @try_except
    def stop(self):
        self._get_yourself().stop()

    @try_except
    def remove(self):
        self._get_yourself().remove()

    @try_except
    def kill(self):
        self._get_yourself().kill()

    @try_except
    def attach_networks(self, networks):
        for network in networks:
            if not DOCKER.networks.list(names=[network]):
                DOCKER.networks.create(network)
            DOCKER.networks.list(names=[network])[0].connect(self._get_yourself())

    def execute(self, command):
        return self._get_yourself().exec_run(command)

    def get_name(self):
        return self.i_name

    def get_logs(self):
        return self._get_yourself().logs()

    def get_id(self):
        return self._get_yourself().id

    def cleanup(self):
        return DockerContainer.rm_networks(self) and DockerContainer.prune_volumes()

    @staticmethod
    def pull_image(image):
        try:
            DOCKER.images.get(image)
        except docker_py.errors.ImageNotFound:
            try:
                DOCKER.images.pull(image)
            except:
                return False
        return True

    def rm_networks(self):
        for network in self.i_networks or ():
            DOCKER.networks.list(names=[network])[0].remove()

    @staticmethod
    def prune_volumes():
        DOCKER.volumes.prune()

    @staticmethod
    def get_container_by_name(name):
        containers = DOCKER.containers.list(filters={"name": name}, all=True)
        for container in containers: # get if name equal, not part
            if container.name == name:
                return container

    @staticmethod
    def get_container_by_id(id_):
        container = DOCKER.containers.list(filters={"id": id_}, all=True)
        return container[0] if container else None


    # private

    def _get_yourself(self):
        return DockerContainer.get_container_by_name(self.i_name)

    def _execute_startup_commands(self):
        for command in self.i_startup_commands:
            error_flag = True
            start_time = time.time()
            while self._get_waiting_time(start_time) >= time.time():
                try:
                    if self.execute(command)[0] == 0:
                        error_flag = False
                        break
                except:
                    pass
                time.sleep(1)
            if error_flag:
                return False
        return True

    def _get_waiting_time(self, start_time):
        return start_time + 60
