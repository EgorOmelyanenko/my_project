import os

import json

from platform_controller.execute import execute
from python_toolkit.filesystem import get_project_directory


class AnsibleConfigurator():
    def __init__(
            self,
            roles_path,
            inventory,
            connection_type,
            role,
            playbook,
            tags="configure",
        ):
        self.i_inventory = inventory
        self.i_connection_type = connection_type
        self.i_role = role
        self.i_tags = tags
        self.i_roles_path = AnsibleConfigurator._get_absolute_path(roles_path)
        self.i_playbook = AnsibleConfigurator._get_absolute_path(playbook)

    async def apply_configuration(self, configuration):
        return await execute(
            "ANSIBLE_ROLES_PATH={} ansible-playbook -i {}, -c {} --tags '{}' --extra-vars 'role={}' --extra-vars '{}' {}".format(
                *self._get_ansible_params(configuration)
            )
        )

# private

    @staticmethod
    def _get_absolute_path(path):
        return path if os.path.isabs(path) else "{}/{}".format(
            get_project_directory(),
            path
        )

    def _get_ansible_params(self, configuration):
        return (
            self.i_roles_path,
            self.i_inventory,
            self.i_connection_type,
            self.i_tags,
            self.i_role,
            json.dumps(configuration),
            self.i_playbook
        )
