import asyncio
import pickle

from platform_controller.components.classes.base_component import BaseComponent
from platform_controller.components.components.application.application.configurator_web_component import ConfiguratorWebApplicationComponent
from platform_controller.components.components.application.application.connector_component import ConnectorComponent
from platform_controller.components.components.application.application.watcher_logs_component import WatcherLogsComponent
from platform_controller.components.components.application.application.watcher_network_component import WatcherNetworkComponent
from platform_controller.components.components.application.docker.nginx_component import NginxComponent
from platform_controller.components.components.application.docker.postgresql_component import PostgresqlComponent
from platform_controller.components.components.application.docker.redis_component import RedisComponent
from platform_controller.components.components.application.docker.tor_component import TorApplicationComponent
from platform_controller.components.components.docker.openvpn_client_component import OpenvpnClientComponent
from platform_controller.components.components.docker.openvpn_server_bridge_component import OpenvpnSeverBridgeComponent
from platform_controller.components.components.docker.openvpn_server_subnet_component import OpenvpnSeverSubnetComponent
from platform_controller.components.components.docker.tor_component import TorComponent
from platform_controller.components.components.host.basic_component import BasicComponent
from platform_controller.components.components.host.interfaces.interfaces_component import InterfacesComponent
from platform_controller.components.components.host.iptables_component import IptablesComponent
from platform_controller.components.components.host.quota_component import QuotaComponent
from platform_controller.components.components.host.routing_component import RoutingComponent
from platform_controller.components.components.host.updates_component import UpdatesComponent
from platform_controller.state import State
from python_toolkit.log import get_log


class Application:

    class ComponentType:
        APPLICATION = 0
        HOST = 1
        CONTAINER = 2

    violation_dependencies_flag = False
    violation_dependencies_list = []


    def __init__(
            self,
            architecture,
            development_localhost_flag,
            development_mount_source_code_flag,
            configurator_web_tag,
            watcher_network_tag,
            watcher_logs_tag,
            connector_tag
        ):

        BaseComponent.initialize(
            architecture,
            development_localhost_flag,
            development_mount_source_code_flag
        )

        State.init_state()

        self.i_lock = asyncio.Lock()
        self.i_container_components = {}
        self.i_host_components = {}
        self.i_application_components = []

        ## components

        # host

        for host_component in [
                BasicComponent(),
                IptablesComponent(),
                QuotaComponent(),
                RoutingComponent(),
                UpdatesComponent(),
                InterfacesComponent()
        ]:
            self._add__component(Application.ComponentType.HOST, host_component)

        # docker

        for docker_component in [
                TorComponent(configurator_web_tag),
                OpenvpnSeverSubnetComponent(configurator_web_tag),
                OpenvpnSeverBridgeComponent(configurator_web_tag),
                OpenvpnClientComponent(configurator_web_tag)
        ]:
            self._add__component(Application.ComponentType.CONTAINER, docker_component)

        # application components

        for application_component in [
                TorApplicationComponent(configurator_web_tag),
                RedisComponent(configurator_web_tag),
                PostgresqlComponent(configurator_web_tag),
                ConfiguratorWebApplicationComponent(configurator_web_tag),
                NginxComponent(configurator_web_tag),
                ConnectorComponent(connector_tag),
                WatcherLogsComponent(watcher_logs_tag),
                WatcherNetworkComponent(watcher_network_tag),
        ]:
            self._add__component(Application.ComponentType.APPLICATION, application_component)


    def start__application(self):
        return False not in [
            self._start__all_application_components(),
            self._start__components_from_state()
        ]

    def stop__application(self):
        return False not in [
            self._stop__all_container_components(),
            self._stop__all_application_components()
        ]

    @State.write_state
    def start__component(self, name, **kwargs):
        return self.i_container_components[name].start(**kwargs)

    @State.write_state
    def stop__component(self, name, **kwargs):
        return self.i_container_components[name].stop(**kwargs)

    @State.write_state
    def remove__component(self, name, **kwargs):
        return self.i_container_components[name].remove(**kwargs)

    @State.write_state
    async def apply_configuration__component(self, name, configuration, **kwargs):
        components = {
            **self.i_container_components,
            **self.i_host_components
        }
        if name in components:
            return await components[name].apply_configuration(configuration, **kwargs)

        return False

    # private

    def _add__component(self, type_, component):
        if type_ == Application.ComponentType.APPLICATION:
            self.i_application_components.append(component)
            return

        components = {
            Application.ComponentType.CONTAINER: self.i_container_components,
            Application.ComponentType.HOST: self.i_host_components,
        }
        components[type_][component.get_name()] = component

    def _start__all_application_components(self):
        for component in self.i_application_components:
            if not component.start():
                self._stop__all_application_components()
                return False

        return True

    def _start__components_from_state(self):
        state = State.get_state()

        for component in state["components"]:
            try:
                state["components"][component] = pickle.loads(state["components"][component])
                state["components"][component].start_from_state()

            except AttributeError:
                get_log().debug("Something went wrong with {} component. Try to clean up the state file and restart app".format(component))
                self.stop__application()
                return False

        self.i_container_components.update(state["components"])

        return True

    def _stop__all_application_components(self):
        return False not in [
            application_component.stop() for application_component in self.i_application_components[::-1]
        ]

    def _stop__all_container_components(self):
        return False not in [
            component.stop() for component in self.i_container_components.values()
        ]

    def _check_dependencies(self):
        for component in self.i_application_components:
            if component.check_dependencies():
                self.violation_dependencies_list.append(component)
