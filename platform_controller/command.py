import subprocess
import threading
import time


class Command(threading.Thread):
    def __init__(self, command):
        super().__init__()

        self.i_process = subprocess.Popen(
            command,
            stdout=subprocess.PIPE
        )
        self.i_buffer = []
        self.i_time = time.time()
        self.i_active_flag = True

    def run(self):
        while self.i_active_flag:
            if not self.i_process:
                time.sleep(0.5)
                continue

            for line in self.i_process.stdout:
                self.i_buffer.append(
                    line.decode().replace("\n", "")
                )

                if time.time() - self.i_time >= 15:
                    self.i_active_flag = False
                    break

    def stop(self):
        if not self.i_process:
            return

        self.i_process.kill()
        self.i_process = None
        self.i_active_flag = False

    def get_active_flag(self):
        return self.i_active_flag

    def get_buffer(self):
        self.i_time = time.time()
        return self.i_buffer
