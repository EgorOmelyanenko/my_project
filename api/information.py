from api.common import ResponseYaml, get_request
from platform_controller.interfaces import get_configuration

class InformationApi:

    @classmethod
    async def information__network_interfaces(cls, _):
        return ResponseYaml(
            get_configuration()
        )

    @classmethod
    async def information__dhcp_leases(cls, _):
        return ResponseYaml()

        # from isc_dhcp_leases import IscDhcpLeases
        # [
        #     {
        #         "mac": lease,
        #         "ip": leases[lease].ip,
        #         "start": DhcpLeasesInstruction._to_localtime(leases[lease].start),
        #         "end": DhcpLeasesInstruction._to_localtime(leases[lease].end),
        #         "uid": leases[lease].data.get("uid", "").strip('"'),
        #     }
        #     for lease in IscDhcpLeases(/var/lib/dhcpd/dhcpd.leases).get_current()
        # ]
