import yaml
from aiohttp.web import Response


class ResponseYaml(Response):
    def __init__(self, data={}, error_flag=False):
        super().__init__(
            text=yaml.dump({
                "data": data,
                "error_flag": error_flag,
            }),
            content_type="application/yaml"
        )


async def get_request(request):
    result = yaml.load(await request.read())
    if "id" in result:
        result["id_"] = result.pop("id")
    return result
