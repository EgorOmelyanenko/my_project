from api.common import ResponseYaml, get_request
from platform_controller.application import Application


class ApplicationApi:

    applciation = None

    @classmethod
    def initialize(
            cls,
            architecture,
            development_localhost_flag,
            development_mount_source_code_flag,
            configurator_web_tag,
            watcher_network_tag,
            watcher_logs_tag,
            connector_tag
    ):
        cls.applciation = Application(
            architecture,
            development_localhost_flag,
            development_mount_source_code_flag,
            configurator_web_tag,
            watcher_network_tag,
            watcher_logs_tag,
            connector_tag
        )

    @classmethod
    def start__application(cls):
        return cls.applciation.start__application()

    @classmethod
    def stop__application(cls):
        return cls.applciation.stop__application()

    @classmethod
    async def start__component(cls, request):
        return ResponseYaml(
            error_flag=not cls.applciation.start__component(
                **(await get_request(request))
            )
        )

    @classmethod
    async def stop__component(cls, request):
        return ResponseYaml(
            error_flag=not cls.applciation.stop__component(
                **(await get_request(request))
            )
        )

    @classmethod
    async def remove__component(cls, request):
        return ResponseYaml(
            error_flag=not cls.applciation.remove__component(
                **(await get_request(request))
            )
        )

    @classmethod
    async def apply_configuration__component(cls, request):
        return ResponseYaml(
            error_flag=not await cls.applciation.apply_configuration__component(
                **(await get_request(request))
            )
        )
