from api.common import ResponseYaml, get_request
from platform_controller.command import Command


class CommandApi:

    commands = {}

    @classmethod
    async def start__command(cls, request):
        uuids = []
        for uuid, command in cls.commands.items():
            if not command.get_active_flag():
                uuids.append(uuid)

        for uuid in uuids:
            del cls.commands[uuid]

        data = (await get_request(request))
        command = Command(data["command"])
        command.start()
        cls.commands[data["uuid"]] = command
        return ResponseYaml()


    @classmethod
    async def stop__command(cls, request):
        data = (await get_request(request))

        if data["uuid"] not in cls.commands:
            return ResponseYaml(error_flag=True)

        cls.commands[data["uuid"]].stop()
        return ResponseYaml()

    @classmethod
    async def get_buffer__command(cls, request):
        data = (await get_request(request))

        if data["uuid"] not in cls.commands:
            return ResponseYaml(error_flag=True)

        return ResponseYaml(
            cls.commands[data["uuid"]].get_buffer()[data["offset"]:]
        )

    @classmethod
    def stop_all__command(cls):
        for _, command in cls.commands.items():
            command.stop()
