import pytest

from fixtures import cleanup, prune, mock_loger, mock_container, mock_state
from common import get_containers
from classes import ApplicationComponentForTesting


# start

def test_start__application_component(cleanup):
    assert ApplicationComponentForTesting().start()

def test_start__application_component__started(cleanup):
    assert not ApplicationComponentForTesting().get_started().start()

def test_start__application_component__stopped(cleanup):
    component = ApplicationComponentForTesting.get_started()
    component.stop()
    assert component.start()

# stop

def test_stop__application_component(cleanup):
    assert ApplicationComponentForTesting.get_started().stop()

def test_stop__application_component_stopped(cleanup):
    component = ApplicationComponentForTesting.get_started()
    component.stop()
    assert not component.stop()

def test_stop__application_component_non_started(cleanup):
    assert not ApplicationComponentForTesting().stop()
