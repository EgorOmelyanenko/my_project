import pytest

from fixtures import mock_loger, mock_state
from platform_controller.application import Application
from platform_controller.components.classes.application_component import ApplicationComponent
from platform_controller.components.classes.multiple_containers_component import MultipleContainersComponent
from platform_controller.components.classes.single_container_component import SingleContainerComponent


def test_start__application():
    assert _get_application().start__application()

def test_stop__application():
    app = _get_started()
    assert app.stop__application()

@pytest.mark.parametrize("kwargs", [
    ({
        "name": "tor",
        "id_": 1
    }),
    ({
        "name": "openvpn_server_subnet",
    }),
])
def test_start_component__application(kwargs):
    app = _get_started()
    assert app.start__component(**kwargs)


@pytest.mark.parametrize("kwargs", [
    ({
        "name": "tor",
        "id_": 1
    }),
    ({
        "name": "openvpn_server_subnet",
    }),
])
def test_stop_component__application(kwargs):
    app = _get_started()
    app.start__component(**kwargs)

    assert app.start__component(**kwargs)


# fixtures

@pytest.fixture(autouse=True)
def mock_components(monkeypatch):
    for method in ["start", "stop", "remove"]:
        for class_ in [ApplicationComponent, MultipleContainersComponent, SingleContainerComponent]:
            if method in dir(class_):
                monkeypatch.setattr(class_, method, lambda *_, **__: True)


# helpers

def _get_started():
    app = _get_application()
    app.start__application()
    return app

def _get_application():
    return Application(
        "x86_64",
        0,
        0,
        "v_0.0.5",
        "v_0.0.3",
        "v_0.0.2",
        "v_0.0.0"
    )
