import pytest

from fixtures import cleanup, prune, mock_loger, mock_container, mock_state
from classes import SingleContainerComponentForTesting, MultipleContainersComponentForTesting


# start

def test_start__container_component(cleanup):
    component = SingleContainerComponentForTesting()
    assert component.start()

def test_start__container_component___started(cleanup):
    assert not SingleContainerComponentForTesting.get_started().start()

def test_start__container_component___stopped(cleanup):
    component = SingleContainerComponentForTesting.get_started()
    component.stop()
    assert component.start()

# stop

def test_stop__container_component(cleanup):
    assert SingleContainerComponentForTesting.get_started().stop()

def test_stop__container_component___stopped(cleanup):
    component = SingleContainerComponentForTesting.get_started()
    component.stop()
    assert not component.stop()

def test_stop__container_component_with_id(cleanup):
    component = MultipleContainersComponentForTesting.get_started(1)
    assert component.stop(id_=1)

def test_stop__container_component_with_id___stopped(cleanup):
    component = MultipleContainersComponentForTesting.get_started(1)
    component.stop(id_=1)
    assert not component.stop(id_=1)

# remove

def test_remove__container_component(cleanup):
    assert SingleContainerComponentForTesting.get_started().remove()

def test_remove__container_component___removed(cleanup):
    component = SingleContainerComponentForTesting.get_started()
    component.remove()
    assert not component.remove()

def test_remove__container_component_with_id(cleanup):
    assert MultipleContainersComponentForTesting.get_started(1).remove(id_=1)

def test_remove__container_component_with_id___removed(cleanup):
    component = MultipleContainersComponentForTesting.get_started(1)
    component.remove(id_=1)
    assert not component.remove(id_=1)
