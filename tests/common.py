import docker as docker_py
from platform_controller.container import DockerContainer


DOCKER = docker_py.from_env()

# helpers

def cleanup():
    _kill()
    _remove()

def prune():
    DockerContainer.prune_volumes()

def _kill():
    if get_containers(False):
        for container in get_containers(False):
            container.kill()

def _remove():
    if get_containers():
        for container in get_containers():
            container.remove()

def get_containers(all_=True):
    result = DOCKER.containers.list(all=all_, filters={"name": "test"})
    return result

def get_ansible_params(inventory, connection_type):
    return (
        "tests/ansible",
        inventory,
        connection_type,
        "test",
        "tests/ansible/playbook.yaml",
    )

def get_started_container():
    container = DockerContainer(
        image="gitlab.nii-ikt.ru/platform/docker_images/alpine.x86_64",
        tag="v_0.0.0",
        name="test",
        entrypoint="sleep 100"
    )
    container.start()
    return container
