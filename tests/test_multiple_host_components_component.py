import pytest
from fixtures import mock_ansible_apply_configuration, mock_loger, mock_state
from classes import MultipleHostComponentsComponentForTesting
from platform_controller.ansible_configurator import AnsibleConfigurator


@pytest.mark.asyncio
@pytest.mark.parametrize("subcomponents", [
    ({
        "test_0": {
            "name": "test_0"
        },
        "test_1": {
            "name": "test_1"
        }
    }),
    ({
        "test_0": {
            "name": "test_0"
        },
        "test_1": {}
    }),
    ({
        "test_3": {
            "name": "test_3"
        },
    }),
    ({})
])
async def test_apply_configuration__multiple_host_components_component(subcomponents):
    assert await MultipleHostComponentsComponentForTesting().apply_configuration(subcomponents)

@pytest.mark.asyncio
async def test_apply_configuration__multiple_host_components_component___negative(monkeypatch):

    async def foo(*_):
        return {
            "exit_code": 1,
            "stdout": "",
            "stderr": ""
        }

    monkeypatch.setattr(AnsibleConfigurator, "apply_configuration", foo)
    assert not await MultipleHostComponentsComponentForTesting().apply_configuration({
        "test_0": {
            "name": "test_0"
        }
    })
