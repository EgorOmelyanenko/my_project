import pytest

from classes import MultipleContainersComponentForTesting, SingleContainerComponentForTesting
from fixtures import mock_ansible_apply_configuration, mock_container, mock_loger, mock_state
from platform_controller.components.classes.host_component import HostComponent

# apply_configuration

@pytest.mark.asyncio
async def test_apply_configuration__host_component(mock_host_component):
    assert await HostComponent().apply_configuration({"name": "test"})

@pytest.mark.asyncio
async def test_apply_configuration__single_container_component():
    assert await SingleContainerComponentForTesting.get_started().apply_configuration({"name": "test"})

@pytest.mark.asyncio
async def test_apply_configuration__multiple_containers_component():
    assert await MultipleContainersComponentForTesting.get_started(id_=1).apply_configuration(
        {"name": "test"},
        id_=1
    )

@pytest.mark.asyncio
async def test_apply_configuration__single_container_component___stopped():
    component = SingleContainerComponentForTesting.get_started()
    component.stop()
    assert not await component.apply_configuration({"name": "test"})

@pytest.mark.asyncio
async def test_apply_configuration__multiple_containers_component___stopped():
    component = MultipleContainersComponentForTesting.get_started(id_=1)
    component.stop(id_=1)
    assert not await component.apply_configuration(
        {"name": "test"},
        id_=1
    )

@pytest.mark.asyncio
async def test_apply_configuration__multiple_containers_component___nonexisten():
    component = MultipleContainersComponentForTesting()
    assert not await component.apply_configuration(
        {"name": "test"},
        id_=1
    )

## fixtures

@pytest.fixture
def mock_host_component(monkeypatch):
    monkeypatch.setattr(
        HostComponent,
        "get_name",
        lambda self: "test"
    )
