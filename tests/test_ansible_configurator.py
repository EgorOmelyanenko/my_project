import pytest

from common import cleanup as _cleanup, get_ansible_params, get_started_container
from platform_controller.ansible_configurator import AnsibleConfigurator


@pytest.mark.parametrize(
    "params",
    [
        get_ansible_params(
            "localhost",
            "local"
        ),
        get_ansible_params(
            get_started_container().get_id(),
            "docker"
        )
    ]
)
@pytest.mark.asyncio
async def test_apply_configuration(params):
    result = await AnsibleConfigurator(*params).apply_configuration({"name": "test"})
    assert result["exit_code"] == 0

# fixtures

@pytest.fixture(scope="module", autouse=True)
def cleanup(request):
    def finalization():
        _cleanup()
    request.addfinalizer(finalization)
