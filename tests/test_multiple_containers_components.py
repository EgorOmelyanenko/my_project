import pytest

from fixtures import mock_loger, mock_container, cleanup, mock_state
from classes import MultipleContainersComponentForTesting

# start

def test_start__multiple_containers_component(cleanup):
    assert MultipleContainersComponentForTesting.get_started(1)

def test_start__multiple_containers_component___multiple_ids(cleanup):
    component = MultipleContainersComponentForTesting.get_started(1)
    assert component.start(2)

def test_start__multiple_containers_component___started(cleanup):
    component = MultipleContainersComponentForTesting.get_started(1)
    assert not component.start(1)

def test_start__multiple_containers_component___count_limit_reached(cleanup):
    component = MultipleContainersComponentForTesting.get_started(1)
    component.set_max_containers_count(1)
    assert not component.start(2)
