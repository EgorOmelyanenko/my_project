import pytest

from common import cleanup as _cleanup, prune as _prune
from python_toolkit.log import LogMock, set_log
from platform_controller.container import DockerContainer
from platform_controller.ansible_configurator import AnsibleConfigurator
from platform_controller.state import State


@pytest.fixture(scope="module", autouse=True)
def mock_loger():
    set_log(LogMock())

@pytest.fixture(autouse=True)
def mock_state(monkeypatch):
    monkeypatch.setattr(State, "get_state", lambda *_, **__: {"components":{}})
    monkeypatch.setattr(State, "_write_state", lambda *_, **__: True)

@pytest.fixture
def cleanup():
    _cleanup()
    yield
    _cleanup()

@pytest.fixture(scope="module", autouse=True)
def prune():
    yield
    _cleanup()
    _prune()

@pytest.fixture(autouse=True)
def mock_container(monkeypatch):
    for method in ["start", "stop", "remove"]:
        monkeypatch.setattr(DockerContainer, method, lambda *_, **__: True)

@pytest.fixture(autouse=True)
def mock_ansible_apply_configuration(monkeypatch):
    async def mock(*_):
        return {
            "exit_code": 0,
            "stdout": "",
            "stderr": ""
        }
    monkeypatch.setattr(AnsibleConfigurator, "apply_configuration", mock)
