import docker as docker_py
import pytest

from common import get_containers
from fixtures import cleanup, prune
from platform_controller.container import DockerContainer


DOCKER = docker_py.from_env()

# instancing

def test_instancing__container(cleanup):
    DockerContainer(
        image="gitlab.nii-ikt.ru/platform/docker_images/alpine.x86_64",
        tag="v_0.0.0",
        name="test"
    )
    assert get_containers()[0]

def test_instancing__container___precreated(cleanup, precreate_container):
    assert DockerContainer(
        image="gitlab.nii-ikt.ru/platform/docker_images/alpine.x86_64",
        tag="v_0.0.0",
        name="test"
    )

# startup_command

def test_startup_command__container___positive(cleanup):
    test_container = DockerContainer(
        image="gitlab.nii-ikt.ru/platform/docker_images/alpine.x86_64",
        tag="v_0.0.0",
        name="test",
        entrypoint="sleep 1000",
        startup_commands=["mkdir /home/test_dir"]
    )
    test_container.start()
    assert test_container.execute("ls /home/test_dir")[0] == 0

def test_startup_command__container___negative(cleanup, monkeypatch):
    test_container = DockerContainer(
        image="gitlab.nii-ikt.ru/platform/docker_images/alpine.x86_64",
        tag="v_0.0.0",
        name="test",
        entrypoint="sleep 1000",
        startup_commands=["ls /home/test_dir"]
    )
    monkeypatch.setattr(test_container, "_get_waiting_time", lambda x: x+5)
    assert not test_container.start()


# fixtures

@pytest.fixture
def precreate_container():
    DOCKER.containers.create(
        image="gitlab.nii-ikt.ru/platform/docker_images/alpine.x86_64:v_0.0.0",
        name="test",
    )
