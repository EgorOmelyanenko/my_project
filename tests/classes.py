from platform_controller.components.classes.base_component import BaseComponent
from platform_controller.components.classes.host_component import HostComponent
from platform_controller.components.classes.multiple_host_components_component import MultipleHostComponentsComponent
from platform_controller.components.classes.single_container_component import SingleContainerComponent
from platform_controller.components.classes.multiple_containers_component import MultipleContainersComponent
from platform_controller.components.classes.application_component import ApplicationComponent


class SingleContainerComponentForTesting(SingleContainerComponent):
    def __init__(self, tag="v_0.0.4"):
        BaseComponent.initialize("x86_64", 0, 0)
        super().__init__(
            "configurator_web/ark_gateway-tor",
            tag,
        )
        self.i_name = "test"

    @staticmethod
    def get_started():
        component = SingleContainerComponentForTesting()
        component.start()
        return component


class MultipleContainersComponentForTesting(MultipleContainersComponent):
    def __init__(self, tag="v_0.0.4"):
        BaseComponent.initialize("x86_64", 0, 0)
        super().__init__(
            "configurator_web/ark_gateway-tor",
            tag,
        )
        self.i_name = "test"

    @staticmethod
    def get_started(id_):
        component = MultipleContainersComponentForTesting()
        component.start(id_)
        return component


class ApplicationComponentForTesting(ApplicationComponent):
    def __init__(self, tag="v_0.0.4"):
        BaseComponent.initialize("x86_64", 0, 0)
        super().__init__(
            "configurator_web/application",
            tag,
        )
        self.i_name = "test"

    @staticmethod
    def get_started():
        component = ApplicationComponentForTesting()
        component.start()
        return component


class MultipleHostComponentsComponentForTesting(MultipleHostComponentsComponent):
    def __init__(self):
        subcomponents = [HostComponentForTesting(), HostComponentForTesting()]

        for id_, subcomponent in enumerate(subcomponents):
            subcomponent.i_name = "test_{}".format(id_)

        super().__init__(subcomponents)

class HostComponentForTesting(HostComponent):
    def __init__(self):
        super().__init__()
        self.i_name = "test"
