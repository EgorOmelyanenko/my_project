#!/bin/sh

cd /usr/bin/ark_gateway/platform_controller
/usr/bin/python3 -m pytest

rm -rf .cache
find . -type d -name __pycache__ -prune -exec rm -rf {} \;

exit 0
