import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_postgresql_install(host):
    assert host.package("postgresql").is_installed


def test_open_postgresql_port(host):
    assert host.socket("tcp://127.0.0.1:5432")


def test_lol(host):
    assert "template0" in host.check_output("psql -h 127.0.0.1 -l -U postgres")
