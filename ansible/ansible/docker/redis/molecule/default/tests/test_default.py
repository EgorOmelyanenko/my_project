import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_redis_install(host):
    assert host.package("redis").is_installed


def test_redis_open_port(host):
    assert host.socket("tcp://127.0.0.1:6379")
