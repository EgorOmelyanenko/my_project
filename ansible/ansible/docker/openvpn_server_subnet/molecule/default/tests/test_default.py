import os
import pytest
import docker
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture
def run_openvpn():
    docker.from_env().containers.get("instance").exec_run(
        cmd="/usr/sbin/openvpn --cd /etc/openvpn --config server.conf",
        detach=True,
    )


def test_openvpn_install(host):
    assert host.package("openvpn").is_installed


def test_open_openvpn_port(host, run_openvpn):
    assert host.socket("tcp://127.0.0.1:1194").is_listening


def test_openvpn_start(host, run_openvpn):
    assert "tun0" in host.check_output("ifconfig")
    assert "10.0.0.1" in host.check_output("ifconfig")
