import os
import docker
import pytest
import subprocess
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture
def create_app():
    subprocess.call(
        "docker cp tests/data/website.py {}:/usr/bin/product/project/website/wsgi.py".format(
            docker.from_env().containers.list()[0].attrs["Id"]
            ),
        shell=True,
    )
    docker.from_env().containers.get("instance").exec_run(
        cmd="/usr/bin/uwsgi --ini /etc/uwsgi/uwsgi.ini",
        detach=True,
    )


def test_uwsgi_open_port(host, create_app):
    assert host.socket("tcp://0.0.0.0:80").is_listening


def test_connect_application(host):
    assert "Hello, World!" in host.check_output("curl 127.0.0.1/hel")
