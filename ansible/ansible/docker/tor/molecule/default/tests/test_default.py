import os
import pytest
import docker
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture
def run_tor():
    docker.from_env().containers.get("instance").exec_run(
        cmd="tor -f /etc/tor/tor.torrc",
        detach=True,
    )

def test_tor_install(host, run_tor):
    assert host.package("tor").is_installed


def test_open_tor_port(host):
    assert host.socket("tcp://127.0.0.1:9050")


def test_open_tor_control_port(host):
    assert host.socket("tcp://127.0.0.1:9051")


def test_use_socks(host):
    assert 0 == host.run("curl --socks5 127.0.0.1:9050 https://google.ru").rc
