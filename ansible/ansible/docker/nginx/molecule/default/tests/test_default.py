import os
import pytest
import docker
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture
def start_nginx():
    docker.from_env().containers.get("instance").exec_run(
        cmd="/usr/sbin/nginx -c /etc/nginx/nginx.conf",
        detach=True,
    )


def test_connect_http_port(start_nginx, host):
    assert host.socket("tcp://127.0.0.1:80").is_listening


def test_connect_https_port(host):
    assert host.socket("tcp://127.0.0.1:443").is_listening


def test_request_to_https_nginx(host):
    assert "404 Not Found" in host.check_output("curl -k https://127.0.0.1:443 -u admin:123456")


def test_request_to_http_nginx(host):
    assert "404 Not Found" in host.check_output("curl http://127.0.0.1:80 -u admin:123456")
