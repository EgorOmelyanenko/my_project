if [ -z "$(pip3 show virtualenv)" ]; then
    pip3 install --user virtualenv
fi

mkdir -p $HOME/workspace/virtualenv/ansible_container

if [ ! -d $HOME/workspace/virtualenv/ansible_container/ansible-container ]; then
    git clone https://github.com/ansible/ansible-container.git $HOME/workspace/virtualenv/ansible_container/ansible-container
else
    cd $HOME/workspace/virtualenv/ansible_container/ansible-container
    git pull https://github.com/ansible/ansible-container.git
fi

virtualenv $HOME/workspace/virtualenv/ansible_container
source $HOME/workspace/virtualenv/ansible_container/bin/activate

pip3 install pip==9.0.3
pip3 install -e $HOME/workspace/virtualenv/ansible_container/ansible-container[docker]
exit 0
